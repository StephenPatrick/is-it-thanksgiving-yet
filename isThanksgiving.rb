# How many leap years happened before this year
def leapYearsBefore(year)
    return (year / 4) - (year / 100) + (year / 400)
end

# If we made this day into a single number of days what number would it be
def dateArrayToNum(dateArray)
    # Thanksgiving began in 1863, so we start counting
    # there.    
    # Each year has 365 days
    returnDate = 365 * ((dateArray[0]+1)-1863)
    
    # unless it is a leap year
    returnDate += leapYearsBefore(dateArray[0]) - leapYearsBefore(1863)  
    
    # We know it is November,
    # And we added this year,
    # So we subtract 61 days for
    # this December and November
    returnDate -= 61
    
    returnDate += dateArray[2]
    
    return returnDate
end

# Is it thursday
def isThursday(dateNum)
    # We know the first thanksgiving was a thursday.
    aThursday = dateArrayToNum([1863,11,26])
    
    # If we subtract the first thanksgiving from
    # our new date, the resultant will be thursday
    # if it is divisible by 7, because every thursday
    # adds seven days to the first thanksgiving date.
    return (dateNum - aThursday) % 7 == 0
end

# Is it thanksgiving
def isThanksgiving(message)
    dateArray = message.split('-')
    
    i = 0
    until i > 2 do
        dateArray[i] = dateArray[i].to_i
        i+=1
    end
    
    # The input should have three things
    if dateArray.size != 3
        puts "Check your input. It should be three integers around hyphens."
        return false
    end
    
    # Thanksgiving takes place in November
    if dateArray[1] != 11
        return false
    end
    
    # It can't be the fourth thursday if it's after the 28th
    if dateArray[2] > 28
        return false
        # It cannot be the 4th thursday if 21 days have 
        # not already happened this month
    elsif dateArray[2] < 22
        return false
    end
    
    # There was a single thanksgiving before 1863.
    # Prior to 1863 it was not annual.
    if dateArray[0] < 1863
        return message == "1789-11-26"
    end
    
    # In about five billion years, the earth is
    # destroyed from the expanding sun, and the
    # United States no longer exists to 
    # celebrate thanksgiving.
    if dateArray[0] > 5000002015
        return false 
    end
    
    dateNum = dateArrayToNum(dateArray)
    if isThursday(dateNum)
        return true
    end
    return false
end